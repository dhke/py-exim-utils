# -*- encoding=utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='py-exim-utils',
    version='0.1.8',
    author='Peter Wullinger',
    author_email='python@dhke.de',
    description='Utilities for the exim MTA',
    long_description='''
Utilities for dealing with the exim MTA,
including parsers for exim-specific file formats.
''',
    url='https://gitlab.com/dhke/py-exim-utils',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'six',
        'pytz'
    ],
    scripts=[
        'bin/exim_get_queue',
    ],
    test_suite='tests.test.suite',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: Artistic License',
        'Operating System :: OS Independent',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Developers',
        'Topic :: Communications :: Email :: Mail Transport Agents',
    ]
)
