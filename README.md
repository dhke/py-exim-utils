
# py-exim-utils [![pipeline status](https://gitlab.com/dhke/py-exim-utils/badges/master/pipeline.svg)](https://gitlab.com/dhke/py-exim-utils/commits/master)

A python library and a set of utilities to interact
with the [exim](http://www.exim.org/) mail transfer agent (MTA).

## Modules

### `exim.db.lsearch`

A parser for
[lsearch](http://www.exim.org/exim-html-current/doc/html/spec_html/ch-file_and_database_lookups.html)-style
lookup files.

### `exim.queue.hfile`

A parser for [`-H`](http://www.exim.org/exim-html-current/doc/html/spec_html/ch-format_of_spool_files.html) spool
metadata files.
