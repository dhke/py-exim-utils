---
layout: default
title: exim.db
---

# `exim.db`

Implements exim "database" access.

Currently, only implements `lookup`
