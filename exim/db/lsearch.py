# -*- encoding=utf-8 -*-

import io
import re
import six
from collections import OrderedDict

__all__ = [
    'LFileParseHandler',
    'LFileParser',
    'LFileWriter',
    'ParseError',
]


def is_hex_digit(s):
    """
        determinate if all characters in `s` are
        heximal digits.
    """
    return all(c in '0123456789abcdefABCDEF' for c in s)


@six.python_2_unicode_compatible
class ParseError(Exception):
    def __init__(self, message, source_name=None, lineno=None):
        self.message = message
        self.source_name = source_name
        self.lineno = lineno

    def __str__(self):
        s = '{0}'.format(self.message)
        if self.source_name or self.lineno:
            s += ' in {0}, line {1}'.format(
                self.source_name or '<unknown>',
                self.lineno or '<unknown>'
            )
        return s

    def __repr__(self):
        return '{0}({1}, source={2}, lineno={3})'.format(
            self.__class__.__name__,
            self.message, self.source_name, self.lineno
        )


class LFileParseHandler(object):
    """
        Base class for lsearch parse handlers.

        Most base classes will want to override `.on_data()`.

        If the content of comments are of interest, `.on_comment()` may also be overridden.
        Comments embedded inside multiline values are reported
        before the associated `.on_data()` event for the key-value pair.

        `.on_key()` and `.on_value()` provide line-level granularity
        for reporting key and value information. Corresponding
        keys and values will be reported with the same index value.
    """
    def on_key(self, key, index, source=None, lineno=None):
        """
            A key has been parsed at the specified location.
        """
        pass

    def on_value(self, value, index, source=None, lineno=None):
        """
            A value has been parsed at the specified location.
            This is always preceeded by an invocation of `.on_key()`
            with a matching index.

            `.on_value()` may be called multiple times for a single index
            value if the value spans multiple lines. In this case
            the line number will indicate the correct source line
            of the value.
        """
        pass

    def on_data(self, key, value, source=None, lineno=None):
        """
            A key value pair has been parsed at the specified
            location.

            Note that `source` is the `source_name` from the parser.
        """
        pass

    def on_comment(self, comment, source=None, lineno=None):
        """
            A comment has been parsed at the specified
            location.

            Note that `source` is the `source_name` from the parser.
        """
        pass


class DictParseHandler(LFileParseHandler):
    """
        A parse handler that stores key-value pairs
        in a dictionary.
    """
    def __init__(self, data=None):
        """
            If data is supplied, it must be a dictionary to store
            key-value pairs returned from the parser.
        """
        if data is None:
            self.data = {}
        else:
            self.data = data

    def on_data(self, key, value, source=None, lineno=None):
        self.data[key] = value


class LFileParserToken(object):
    """
        A parser token.
    """
    __slots__ = [
        'name',
        'value',
        'start_lineno',
    ]

    def __init__(self, name, value, start_lineno):
        self.name = name
        self.value = value
        self.start_lineno = start_lineno

    def __bool__(self):
        return bool(self.name)

    # python2
    def __nonzero__(self):
        return 1 if self.name else 0

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return '{}({!r}, {!r}, {!r})'.format(
            self.__class__.__name__,
            self.name,
            self.value,
            self.start_lineno)


class LFileParseContext(object):
    """
        A holder object for the current parse state.

        Records
        - the current source object (`source`)
        - the name of the current source (`source_name`).
        - the current (line) buffer holding the unconsumed bytes
          from the current line (`_buffer`)
        - the current line number (`lineno`)

        The main method of this class is `.read_token()`
        used to obtain the next, named token from the input
        stream.
    """
    tokens = OrderedDict((
        ('key', b'[^":\\s]+'),
        ('quotedkey', b'"(?:\\.|[^"])+"'),
        ('colon', b':'),
        # space includes eol, so pick it first
        ('eol', b'(?:\\r\\n?|\\n\\r?)'),
        ('space', b'\\s+'),
        ('value', b'.+$'),
        ('continuation', b'^\\s+'),
        ('comment', b'^#.*$'),
    ))

    def __init__(self, source, source_name=None, lineno=0, debug=False):
        """
            Create a new parser context.

            - `source` must be a stream-like object supporting
              a `.readline()` method to obtain the next input line.
            - `source_name` represents the name of the source
              object to be used in error messages and other output.
              Defaults to `source`.
            - `lineno` is the number of the last line read.
               Defaults to 0, i.e. no lines have been read, yet.
        """
        self.source = source
        self.source_name = source_name or source
        self.lineno = lineno
        self._buffer = None
        self.debug = debug
        self.__expr_cache = {}

    def print_debug(self, message):
        if self.debug:
            print(message)

    def _compile_token_re(self, token_names):
        """
            Given a list of token names, compile a single regular
            expression pattern for matching these tokens.

            Returns a regular expression pattern object.

            Partial regular expressions are picked up from the
            `tokens` dictionary of the current context object.

            Token names must refer to existing keys of `token` dictionary.
            It is an error to specify an unknown token name.
            The token name 'EOF' is special cased and will be ignored
            when constructing the regular expression.

            The regular expressdion will contain named groups
            same names as the specified tokens. Groups will
            appear in the same order as the token names.
        """
        token_names = tuple(token_names)
        expression = self.__expr_cache.get(token_names, None)
        if expression is None:
            expressions = []
            for token_name in token_names:
                if token_name != 'EOF':
                    token_re = self.tokens.get(token_name, None)
                    if token_re is None:
                        raise ValueError("Unknown token name: '{}'".format(token_name))
                    expressions.append(b'(?P<' + token_name.encode('ascii') + b'>' + token_re + b')')
            expression = b'(?:' + b'|'.join(expressions) + b')'
            self.__expr_cache[token_names] = expression
        return re.compile(expression)

    def _clear_expression_cache(self):
        self.__expr_cache = {}

    def get_line(self):
        """
            Retrieve a line from the input source.

            If the current buffer has not yet been fully consumed,
            the current buffer is returned. If the current
            buffer has already been fully consumed (including the trailing newline),
            a new line will be read from the input source.

            An empty buffer is returned, when the current
            buffer has been fully consumed and there is
            not more data available from the input source (EOF).
        """
        if not self._buffer:
            self._buffer = self.source.readline()
            self.print_debug('L: {!r}'.format(self._buffer))
            self.lineno += 1
        return self._buffer

    def consume(self, size):
        """
            Consume up to `size` bytes from the current buffer.

            If the current buffer contains less than `size` bytes
            the whole buffer is consumed.
        """
        self.print_debug('C {!r}:{!r}'.format(self._buffer[size:], self._buffer[:size]))
        self._buffer = self._buffer[size:]

    def read_token(self, *token_names, **kwargs):
        """
            Read a token from the input stream.

            Takes as input the names of expected tokens. If a token could be matched,
            a token object containing the name, the value, and the starting
            line number of the token is returned. If a token was successfully matched,
            the characters/bytes representing the token are consumed
            from the input buffer.

            If `optional` is `False` (the default) and none of the expected tokens
            could be parsed at the current input position, `ParseError` is raised.

            If `optional` is `True`, a token with name `None` and an empty value
            is returned.

            Note that only the current buffer is considered.
            Tokens are not parsed across multiple buffer reads (i.e. input lines).
        """
        optional = kwargs.get('optional', False)
        token_names = list(token_names)
        allow_eof = optional or ('EOF' in token_names)
        expr = self._compile_token_re(token_names)

        line = self.get_line()
        start_lineno = self.lineno
        # EOF
        if not line:
            if allow_eof:
                token = LFileParserToken('EOF', None, start_lineno)
                self.print_debug('T: {!r}'.format(token))
                return token
            else:
                raise ParseError(
                    'Expected one of ({}) at {} (got EOF)'.format(', '.join(token_names), line),
                    source_name=self.source_name,
                    lineno=self.lineno
                )

        match = expr.match(line)
        if match:
            groups = {key: value for key, value in match.groupdict().items() if value is not None}
            assert 1 == len(groups)
            token_name = next(iter(groups.keys()))
            # consume the buffer
            self.consume(match.end())
            token = LFileParserToken(token_name, groups[token_name], start_lineno)
        elif optional:
            token = LFileParserToken(None, type(line)(), start_lineno)
        else:
            raise ParseError(
                'Expected one of {} at {}'.format(', '.join(token_names), line),
                source_name=self.source_name,
                lineno=self.lineno
            )

        return token


class LFileParser(object):
    """
        A parser for exim "lsearch" format lookup files.

        - `handler`: An object providing the same interface
          as `LFileParseHandler`. Parsed input will be reported
          to the handler.

        - `encoding`: if specified, the decoded data is
          decoded using the specified encoding.
          this case (unicode) strings are reported
          to the handler instead of raw bytes.

        Actual parsing is initiated using one of
        `.parse_string()`, `.parse_bytes()`, or `.parse()`.
    """
    def __init__(self, handler, encoding=None, debug=False):
        self.handler = handler
        self.encoding = encoding
        self.debug = debug

    def parse_bytes(self, b, source_name=None):
        """
            Parse from a byte string
        """
        source = io.BytesIO(b)
        source_name = source_name or b
        return self.parse(source, source_name=source_name)

    def parse_string(self, s, source_name=None, intermediate_encoding=None):
        """
            Parse from an unicode string.

            Since the parser operators on raw byte strings, only.
            the input string will be encoded into a byte string
            using `intermediate_encoding` (default: 'utf-8').
        """
        source_name = source_name or s
        intermediate_encoding = intermediate_encoding or 'utf-8'
        source = s.encode(intermediate_encoding)
        return self.parse_bytes(source, source_name=source_name)

    def parse_comment(self, context):
        context.print_debug('parse_comment')
        # comments are always optional
        token = LFileParserToken('comment', None, context.lineno)
        while token.name in ('eol', 'comment'):
            token = context.read_token('EOF', 'eol', 'comment', optional=True)
            if token.name == 'comment':
                assert token.value[0:1] == b'#'
                self.on_comment(token.value[1:], source=context.source_name, lineno=token.start_lineno)
                token = context.read_token('EOF', 'eol')
        return token.name == 'EOF'

    def parse_quoted_key(self, token, context):
        def get_replacement(match):
            value = match.group(1)
            if value[:1] == b'n':
                repl = b'\n'
            elif value[1:] == b'r':
                repl = b'\r'
            elif value[1:] == b't':
                repl = b'\t'
            elif value[1:] == b'x':
                repl = bytes([int(match.group(1), 16)])
            elif value[1:] in b'01234567':
                repl = bytes([int(match.group(1), 8)])
            else:
                repl = match.group(1)
            return repl

        context.print_debug('parse_quoted_key')
        assert token.value[:1] == b'"'
        assert token.value[-1:] == b'"'
        quote_pattern = b'\\\\(n|r|t|x[01234567890abcdefABCDEF]{1,2}|[01234567]{1,3})'
        unquoted_value, nrepl = re.subn(quote_pattern, get_replacement, token.value[1:-1])
        quoted_key = LFileParserToken('quoted_key', unquoted_value, token.start_lineno)
        return quoted_key

    def parse_value(self, context, start_lineno, need_continuation=False):
        context.print_debug('parse_value')
        have_continuation = True
        value_parts = []
        while have_continuation:
            have_continuation = False
            if need_continuation:
                # require initial continuation (i.e. after EOF)
                token = context.read_token('continuation', optional=True)
                if not token:
                    break
                need_continuation = False

            token = context.read_token('EOF', 'eol', 'value')
            if token.name == 'value':
                token.value = token.value.strip()
                value_parts.append(token)
                token = context.read_token('EOF', 'eol')
            if token.name == 'eol':
                self.parse_comment(context)
                token = context.read_token('continuation', optional=True)
                have_continuation = bool(token)
        return value_parts

    def parse_key(self, context):
        context.print_debug('parse_key')
        token = context.read_token('key', 'quotedkey')
        if token.name == 'key':
            key = token
        elif token.name == 'quotedkey':
            key = self.parse_quoted_key(token, context)
        return key

    def parse(self, source, source_name=None):
        """
            - `source` must be a stream-like object supporting
              a `.readline()` method to obtain the next input line.
            - `source_name` represents the name of the source
              object to be used in error messages and other output.
              Defaults to `source`.

            Parsed data is reported to the parser's handler.
        """
        source_name = source_name or source
        context = LFileParseContext(source, source_name=source_name, lineno=0, debug=self.debug)

        index = 0
        eof = False
        while not eof:
            eof = self.parse_comment(context)
            if not eof:
                key = self.parse_key(context)
                self.on_key(key.value, index, source=context.source_name, lineno=key.start_lineno)

                # keys are terminated by space, a colon, EOF or eol.
                # if space is present, it may be followed by a colon
                for token_name in ('space', 'colon'):
                    token = context.read_token('EOF', 'eol', token_name, optional=True)
                    if token.name in ('EOF', 'eol'):
                        break
                # if the key was terminated by eol,
                # we need a continuation marker before
                # we accept a value on the next line.
                if token.name == 'eol':
                    need_continuation = True
                    eof = self.parse_comment(context)
                else:
                    need_continuation = False

                value_parts = []
                if not eof:
                    value_parts = self.parse_value(
                        context,
                        start_lineno=key.start_lineno,
                        need_continuation=need_continuation
                    )

                if value_parts:
                    for value in value_parts:
                        self.on_value(
                            value.value, index,
                            source=context.source_name,
                            lineno=value.start_lineno,
                        )
                    value_joined = b' '.join(token.value for token in value_parts)
                    self.on_data(key.value, value_joined, source=context.source_name, lineno=key.start_lineno)
                else:
                    self.on_data(key.value, None, source=context.source_name, lineno=key.start_lineno)
                index += 1

    def _maybe_decode(self, value):
        if value is None:
            return value
        elif self.encoding:
            return value.decode(self.encoding)
        else:
            return value

    def on_key(self, key, index, source=None, lineno=None):
        self.handler.on_key(
            self._maybe_decode(key), index,
            source=source,
            lineno=lineno
        )

    def on_value(self, value, index, source=None, lineno=None):
        self.handler.on_value(
            self._maybe_decode(value), index,
            source=source,
            lineno=lineno
        )

    def on_data(self, key, value, source=None, lineno=None):
        self.handler.on_data(
            self._maybe_decode(key),
            self._maybe_decode(value),
            source=source,
            lineno=lineno,
        )

    def on_comment(self, comment, source=None, lineno=None):
        self.handler.on_comment(
            self._maybe_decode(comment),
            source=source,
            lineno=lineno,
        )


class LFileWriter(object):
    EOL_PATTERN = re.compile(b'(?:\\r\\n?|\\n\\r?)')

    def __init__(self, target, encoding=None, debug=False):
        self.target = target
        self.encoding = encoding
        self.debug = debug

    def _maybe_encode(self, value):
        if isinstance(value, six.string_types) and self.encoding:
            return value.encode(self.encoding)
        else:
            return value

    def escape_key(self, key):
        if any(char in key for char in b'\\\n\r\t '):
            key = key.replace(b'\\', b'\\\\')
            key = key.replace(b'\n', b'\\n')
            key = key.replace(b'\r', b'\\r')
            key = key.replace(b'\t', b'\\t')
            key = key.replace(b'"',  b'\\"')
            key = b'"' + key + b'"'
        return key

    def escape_value(self, value):
        value, nsub = self.EOL_PATTERN.subn(b'\0 ', value)
        return value

    def write_raw(self, *items):
        for item in items:
            self.target.write(item)

    def write_comment(self, comment):
        comment = self._maybe_encode(comment)
        for comment_line in self.EOL_PATTERN.split(comment):
            self.write_raw(b'#', comment_line, b'\n')

    def write_data(self, key, value=None):
        self.write_raw(self.escape_key(self._maybe_encode(key)))
        if value is not None:
            self.write_raw(b':')
            self.write_raw(self.escape_value(self._maybe_encode(value)))
        self.write_raw(b'\n')
