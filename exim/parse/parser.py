# -*- encoding=utf-8 -*-

class InputToken(object):
    def __init__(self, type, value, source=None, lineno=None):
        self.value = value
        self.source = source
        self.lineno = lineno

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return '{}({}, {}, {})'.format(
            self.__class__.__name__,
            self.value,
            self.source,
            self.lineno
        )


class TokenSource(object):
    tokens = OrderedDict((
        ('key', b'[^":\\s]+'),
        ('quotedkey', b'"(?:\\.|[^"])+"'),
        ('colon', b':'),
        # space includes eol, so pick it first
        ('eol', b'(?:\\r\\n?|\\n\\r?)'),
        ('space', b'\\s+'),
        ('value', b'.+$'),
        ('continuation', b'^\\s+'),
        ('comment', b'^#.*$'),
    ))

    def parse_bytes(self, b, source_name=None):
        """
            Parse from a byte string
        """
        source = io.BytesIO(b)
        source_name = source_name or b
        return self.parse(source, source_name=source_name)

    def parse_string(self, s, source_name=None, intermediate_encoding=None):
        """
            Parse from an unicode string.

            Since the parser operators on raw byte strings, only.
            the input string will be encoded into a byte string
            using `intermediate_encoding` (default: 'utf-8').
        """
        source_name = source_name or s
        intermediate_encoding = intermediate_encoding or 'utf-8'
        source = s.encode(intermediate_encoding)
        return self.parse_bytes(source, source_name=source_name)

    def parse(self, source, source_name=None):

