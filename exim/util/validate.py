# -*- encoding=utf-8 -*-

import re

LOCAL_PART_PATTERN = b'(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")'
FQDN_PATTERN = b'(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)*[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)'
# the commented version does not accept non-fqdn hostnames
FQDN_ADDRESS_PATTERN = (
    b'^(?P<local_part>' + LOCAL_PART_PATTERN + b')'
    + b'@'
    + b'(?P<domain>' + FQDN_PATTERN + b')'
    + b'$'
)
DOMAIN_PATTERN = b'(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)*[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'

ADDRESS_PATTERN = (
    b'^(?P<local_part>' + LOCAL_PART_PATTERN + b')'
    + b'@'
    + b'(?P<domain>' + DOMAIN_PATTERN + b')'
    + b'$'
)


def is_valid_email_address(addr, require_fqdn=True, ignore_case=True, accept_empty=False):
    if ignore_case:
        flags = re.I
    else:
        flags = 0
    if accept_empty and addr == b'':
        return True
    elif require_fqdn:
        return bool(re.match(FQDN_ADDRESS_PATTERN, addr, flags=flags))
    else:
        return bool(re.match(ADDRESS_PATTERN, addr, flags=flags))
