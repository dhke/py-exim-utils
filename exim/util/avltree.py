# -*- encoding=utf-8 -*-

"""
    Pure python implementation of a balanced binary tree (AVL tree)
"""

PRE_ORDER = 'nlr'
IN_ORDER = 'lnr'
POST_ORDER = 'lrn'


class Node(object):
    __slots__ = ['balance', 'value', 'children']

    def __init__(self, value, left=None, right=None, balance=0, children=None):
        self.value = value
        if children is not None:
            self.children = children
        else:
            self.children = [left, right]
        self.balance = balance

    def as_list(self):
        return [self.balance, self.value] + \
            [(child.as_list() if child else child) for child in self.children]

    def __repr__(self):
        return 'Node({!r}, balance={!r})'.format(self.value, self.balance)

    def copy_tree(self):
        children_copy = [
            (child.copy_tree() if child is not None else None)
            for child in self.children
        ]
        return self.__class__(
            self.value,
            children=children_copy,
            balance=self.balance
        )


class Tree(object):
    def __init__(self, seq=None):
        self.root = Node(None)
        if seq:
            self.extend(seq)

    def _rotate_left(self, root):
        assert root.balance == 2
        right = root.children[1]
        assert right is not None
        root.children[1] = right.children[0]
        right.children[0] = root
        if right.balance == 0:
            root.balance = 1
            right.balance = -1
        else:
            root.balance = 0
            right.balance = 0
        return right

    def _rotate_right(self, root):
        assert root.balance == -2
        left = root.children[0]
        assert left is not None
        root.children[0] = left.children[1]
        left.children[1] = root
        if left.balance == 0:
            root.balance = -1
            left.balance = 1
        else:
            root.balance = 0
            left.balance = 0
        return left

    def _rotate_right_left(self, root):
        assert root.balance == 2
        right = root.children[1]
        rl = right.children[0]
        rll, rlr = rl.children
        right.children[0] = rlr
        rl.children[1] = right
        root.children[1] = rll
        rl.children[0] = root

        if rl.balance > 0:
            root.balance = -1
            right.balance = 0
        else:
            if rl.balance == 0:
                root.balance = 0
                right.balance = 0
            else:
                root.balance = 0
                right.balance = 1
        rl.balance = 0
        return rl

    def _rotate_left_right(self, root):
        assert root.balance == -2
        left = root.children[0]
        lr = left.children[1]
        lrl, lrr = lr.children
        left.children[1] = lrl
        lr.children[0] = left
        root.children[0] = lrr
        lr.children[1] = root

        if lr.balance < 0:
            root.balance = 1
            left.balance = 0
        else:
            if lr.balance == 0:
                root.balance = 0
                left.balance = 0
            else:
                root.balance = 0
                left.balance = -1
        lr.balance = 0
        return lr

    def find_node_path(self, value):
        path = [(self.root, 0)]
        node = self.root.children[0]
        while node is not None:
            if value < node.value:
                path.append((node, 0))
                node = node.children[0]
            elif value > node.value:
                path.append((node, 1))
                node = node.children[1]
            else:
                return node, path
        return None, path

    def find_node(self, value):
        node = self.root.children[0]
        while node is not None:
            if value < node.value:
                node = node.children[0]
            elif value > node.value:
                node = node.children[1]
            else:
                return node
        return None

    def _rebalance(self, parent):
        """
            If parent is out of balance,
            rebalance the tree below parent.

            Returns a tuple `(root, child_grown)`
            with `root` being the new root of the subtree
            at parent and `rebalanced` a `boolean`,
            indicating the a rebalancing was performed.
            In case of a rebalancing, the height of the
            subtree has not changed, i.e. it is the
            same as before the immediately
            preceeding insertion/deletion that made
            the rotation necessary.
        """
        # rebalancing is needed, but we never re-balance
        # the root pseudo-node
        rebalanced = (
            parent is not self.root
            and (parent.balance < -1 or parent.balance > 1)
        )
        if rebalanced:
            if parent.balance > 1:
                assert parent.balance == 2
                if parent.children[1].balance >= 0:
                    parent = self._rotate_left(parent)
                else:
                    parent = self._rotate_right_left(parent)
            elif parent.balance < -1:
                assert parent.balance == -2
                if parent.children[0].balance <= 0:
                    parent = self._rotate_right(parent)
                else:
                    parent = self._rotate_left_right(parent)

        return parent, rebalanced

    def add(self, value):
        path = [(self.root, 0)]

        node, path = self.find_node_path(value)
        if node is not None:
            raise ValueError('Value already in tree')

        if node is None:
            new_node = child = Node(value)
            child_grown = True
            while path:
                parent, child_index = path.pop()
                parent.children[child_index] = child
                assert child_index == 0 or child_index == 1

                if child_grown:
                    if child_index == 0:
                        parent.balance -= 1
                        child_grown = parent.balance < 0
                    else:
                        parent.balance += 1
                        child_grown = parent.balance > 0

                parent, rebalanced = self._rebalance(parent)
                if rebalanced:
                    child_grown = not rebalanced
                child = parent
        return new_node

    def extend(self, values):
        for value in values:
            self.add(value)

    def __add__(self, seq):
        tree = self.copy()
        tree.extend(seq)
        return tree

    def _find_for_removal(self, value):
        node, path = self.find_node_path(value)
        if node is None:
            raise KeyError('Value not in tree', value)

        parent, child_index = path[-1]
        if node.children[0] is not None and node.children[1] is not None:
            # node has two children
            assert node.children[1] is not None
            path.append((node, 1))
            # walk down to immediate successor, i.e. right, left*
            succ = node.children[1]
            while succ.children[0] is not None:
                path.append((succ, 0))
                succ = succ.children[0]
            # swap values of node (to remove) with
            # its immediate successor
            node.value = succ.value
            node = succ

        return node, path

    def remove(self, value):
        node, path = self._find_for_removal(value)

        if node.children[0] is None:
            child = node.children[1]
        elif node.children[1] is None:
            child = node.children[0]
        else:
            child = None

        # propagate size change
        child_shrunk = True
        while path:
            parent, child_index = path.pop()
            parent.children[child_index] = child

            if child_shrunk:
                if child_index == 0:
                    parent.balance += 1
                    child_shrunk = parent.balance == 0
                else:
                    parent.balance -= 1
                    child_shrunk = parent.balance == 0

                parent, rebalanced = self._rebalance(parent)
                if rebalanced:
                    child_shrunk = not rebalanced

            child = parent

        return node

    def as_list(self):
        if self.root.children[0]:
            return self.root.children[0].as_list()
        else:
            return None

    def node_iter(self):
        return TreeNodeIterator(self)

    def copy(self):
        tree = self.__class__()
        tree.root = self.root.copy_tree()
        return tree

    def __copy__(self):
        return self.copy()

    def __contains__(self, value):
        return self.find_node(value) is not None

    def __iter__(self):
        return TreeValueIterator(self)

    def __len__(self):
        n = 0
        for i in self:
            n += 1
        return n

    def __bool__(self):
        return self.root.children[0] is not None

    def __repr__(self):
        return '{}({!r})'.format(self.__class__.__name__, list(self))

    def __str__(self):
        return str(list(self))


class TreeNodeIterator(object):
    def __init__(self, tree, order=IN_ORDER):
        self.order = order
        if tree.root.children[0] is not None:
            self._stack = [(tree.root.children[0], 0)]
        else:
            self._stack = []

    def __iter__(self):
        return self

    def next(self):
        return self.__next__()

    def __next__(self):
        order = self.order
        stack = self._stack

        if not stack:
            raise StopIteration()

        while stack:
            p = len(order)
            while stack and p >= len(order):
                node, p = stack.pop()
            if not stack and p >= len(order):
                raise StopIteration()

            next_op = order[p]
            if next_op == 'n':
                stack.append((node, p + 1))
                return node
            elif next_op == 'l':
                stack.append((node, p + 1))
                if node.children[0] is not None:
                    stack.append((node.children[0], 0))
            elif next_op == 'r':
                stack.append((node, p + 1))
                if node.children[1] is not None:
                    stack.append((node.children[1], 0))


class TreeValueIterator(object):
    def __init__(self, tree, order=IN_ORDER):
        self.node_iter = TreeNodeIterator(tree, order=order)

    def next(self):
        return self.__next__()

    def __next__(self):
        return next(self.node_iter).value
