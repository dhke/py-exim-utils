# -*- encoding=utf-8 -*-

__all__ = [
    'ignore_errno',
]

from contextlib import contextmanager


@contextmanager
def ignore_errno(errno0, *extra_errnos):
    errnos = [errno0]
    errnos.extend(extra_errnos)

    try:
        yield True
    except EnvironmentError as ex:
        if ex.errno not in errnos:
            raise
