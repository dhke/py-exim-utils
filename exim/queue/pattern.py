# -*- encoding=utf-8 -*-

__all__ = [
    'FilterPattern',
    'RegexFilterPattern',
    'FnMatchFilterPattern',
    'HFileMatcher',
    'parse_pattern',
]

import re
import six
import fnmatch


class FilterPattern(object):
    def __init__(self, pattern, strip_value=False, encoding='utf-8'):
        self.strip_value = strip_value
        self.encoding = encoding
        decoded_pattern = self.decode_pattern(pattern)
        self.parse_pattern(decoded_pattern)

    def decode_pattern(self, pattern):
        if isinstance(pattern, six.binary_type):
            return pattern
        else:
            return pattern.encode(self.encoding)

    def parse_pattern(self, pattern):
        self.pattern = pattern

    def match(self, s):
        return True

    def prepare_value(self, s):
        if self.strip_value:
            if isinstance(self.strip_value, six.binary_type):
                return s.strip(self.strip_value)
            else:
                return s.strip()
        else:
            return s


class RegexFilterPattern(FilterPattern):
    def __init__(self, pattern, ignore_case=False, **kwargs):
        self.ignore_case = ignore_case
        super(RegexFilterPattern, self).__init__(pattern, **kwargs)

    def parse_pattern(self, pattern):
        if self.ignore_case:
            self.pattern = re.compile(pattern, re.I)
        else:
            self.pattern = re.compile(pattern)

    def match(self, value):
        value = self.prepare_value(value)
        print("V=", value, "P=", self.pattern, "T=", self.pattern.match(value))
        return self.pattern.match(value)


class FnMatchFilterPattern(FilterPattern):
    def parse_pattern(self, pattern):
        self.pattern = pattern.lower()

    def prepare_value(self, value):
        value = super(FnMatchFilterPattern, self).prepare_value(value)
        value = value.lower()
        return value

    def match(self, value):
        value = self.prepare_value(value)
        return fnmatch.fnmatchcase(value, self.pattern)


class ContainsFilterPattern(FilterPattern):
    def parse_pattern(self, pattern):
        self.pattern = pattern.lower()

    def prepare_value(self, value):
        value = super(ContainsFilterPattern, self).prepare_value(value)
        value = value.lower()
        return value

    def match(self, value):
        value = self.prepare_value(value)
        return self.pattern in value


class HFileHeaderFilter(object):
    def __init__(self, filters=None):
        if getattr(filters, 'items'):
            self.filters = [
                (field_pattern, value_pattern)
                for field_pattern, value_patterns in filters.items()
                for value_pattern in value_patterns
            ]
        else:
            self.filters = [
                (field_pattern, value_pattern)
                for field_pattern, value_patterns in list(filters)
                for value_pattern in value_patterns
            ]

    def add_filter(self, field_filter=None, value_filter=None):
        field_filter = field_filter or None
        self.filters.append((field_filter, value_filter))

    def match(self, hfile_header):
        for field_filter, value_filter in self.filters:
            if field_filter is None or field_filter.match(hfile_header.field):
                return value_filter.match(hfile_header.value)
            else:
                return False


class HFileMatcher(object):
    def __init__(
        self,
        env_from=None,
        env_rcpt=None,
        header=None,
    ):
        self.env_from = env_from
        self.env_rcpt = env_rcpt
        self.header = header

    def match(self, hfile):
        return (
            (not self.env_from or self.env_from.match(hfile.env_from))
            and (not self.env_rcpt or any(
                self.env_rcpt.match(rcpt.address)
                for rcpt in hfile.recipients
            ))
            and (not self.header
                or any(
                    self.header.match(header)
                    for header in hfile.headers
                )
            )
        )


def parse_pattern(pattern_str):
    if pattern_str[:3] in (b're:', 're:'):
        return RegexFilterPattern(pattern_str[3:])
    elif pattern_str[:4] in (b'ire:', 'ire:'):
        return RegexFilterPattern(pattern_str[4:])
    elif pattern_str[:6] in (b'match:', 'match:'):
        return FnMatchFilterPattern(pattern_str[6:])
    else:
        return ContainsFilterPattern(pattern_str)
