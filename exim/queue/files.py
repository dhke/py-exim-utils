# *- encoding=utf-8 -*-

__all__ = [
    'get_hfile_paths',
    'read_hfiles',
]


import errno
import os

from exim.conf import get_conf, CONF_SPOOLDIR
from exim.queue.hfile import HFileParser
from exim.util.errno import ignore_errno


def get_hfile_paths(spool_dir=None):
    spool_dir = spool_dir or os.path.join(get_conf(CONF_SPOOLDIR), 'input')

    for name in os.listdir(spool_dir):
        if name.endswith('-H'):
            yield os.path.join(spool_dir, name)


def read_hfiles(hfile_paths, ignore_missing=True):
    parser = HFileParser()
    for hfile_path in hfile_paths:
        if ignore_missing:
            with ignore_errno(errno.ENOENT):
                with open(hfile_path, 'rb') as hfile_fp:
                    hfile = parser.parse(hfile_fp, hfile_path)
                    yield hfile
        else:
            with open(hfile_path, 'rb') as hfile_fp:
                yield hfile
