# -*- encoding=utf-8 -*-

from collections import OrderedDict
import contextlib
import io
import re
import six
from datetime import datetime
import pytz

from ..util import avltree
from ..util.validate import is_valid_email_address


# these are synchronized with exim's src/spool_in.c as of x v4.91
HFILE_VARIABLES = {
    b'active_hostname':             1,
    b'allow_unqualified_recipient': 0,
    b'allow_unqualified_sender':    0,
    b'auth_id':                     1,
    b'auth_sender':                 1,
    b'active_hostname':             1,
    b'body_linecount':              1,
    b'body_zerocount':              1,
    b'bmi_verdicts':                1,
    b'deliver_firsttime':           0,
    b'dsn_ret':                     1,
    b'dsn_envid':                   1,
    b'frozen':                      0,
    b'helo_name':                   1,
    b'host_lookup_deferred':        0,
    b'host_lookup_failed':          0,
    b'host_address':                1,
    b'host_auth':                   1,
    b'host_lookup_failed':          0,
    b'host_name':                   1,
    b'ident':                       1,
    b'interface_address':           1,
    b'local_scan':                  1,
    b'local':                       0,
    b'localerror':                  0,
    b'manual_thaw':                 0,
    b'N':                           0,
    b'received_protocol':           1,
    b'received_time_usec':          1,
    b'sender_set_untrusted':        1,
    b'spam_bar':                    1,
    b'spam_score':                  1,
    b'spam_score_int':              1,
    b'spool_file_wireformat':       0,
    b'smtputf8':                    0,
    b'tls_certificate_verified':    0,
    b'tls_ourcert':                 1,
    b'tls_peercert':                1,
    b'tls_cipher':                  1,
    b'tls_sni':                     1,
    b'tls_ocsp':                    1,
    b'tls_requiretls':              1,
    b'tls_peerdn':                  1,
    b'utf8_downcvt':                0,
    b'utf8_optdowncvt':             0,
}

HFILE_VARIABLE_TYPE = {
    'queue',
    'old_acl',
    'connection',
    'message',
}


class ParseError(Exception):
    def __init__(self, message, source_name=None, offset=None):
        self.message = message
        self.source_name = source_name
        self.offset = offset

    def __str__(self):
        s = '{0}'.format(self.message)
        if self.source_name or self.offset:
            s += ' in {0}, offset {1}'.format(
                self.source_name or '<unknown>',
                self.offset or '<unknown>'
            )
        return s

    def __repr__(self):
        return '{0}({1!r}, source={2!r})'.format(
            self.__class__.__name__,
            self.message, self.source_name
        )


@six.python_2_unicode_compatible
class ValidationError(Exception):
    def __init__(self, message, fields=None):
        self.message = message
        self.fields = fields

    def __str__(self):
        return self.message

    def __repr__(self):
        return '{0}({1!r}, {2!r})'.format(
            self.__class__.__name__,
            self.message,
            self.fields,
        )


@six.python_2_unicode_compatible
class HFileHeader(object):
    HEADER_FLAGS = {
        b'B': 'Bcc',
        b'C': 'Cc',
        b'F': 'From',
        b'I': 'Message-ID',
        b'P': 'Received',
        b'R': 'Reply-To',
        b'S': 'Sender',
        b'T': 'To',
        b'*': '__REWRITTEN__',
        b' ': '__NOSPECIAL__',
    }

    def __init__(self, flag, field, value):
        self.flag = flag
        self.field = field
        self.value = value

    def __repr__(self):
        return '{0}({1!r}, {2!r}, {3!r})'.format(
            self.__class__.__name__,
            self.flag, self.field, self.value
        )

    def __str__(self):
        return '{}:{}'.format(self.field.decode('ascii'), self.value.decode('ascii'))

    def bytes(self):
        header = self.field + b':' + self.value
        length_str = '{!s:0>3}'.format(len(header)).encode('ascii')
        return length_str + self.flag + b' ' + header


class HFileRecipient(object):
    def __init__(
        self,
        address,
        errors_to_address=None,
        original_recipient=None
    ):
        self.address = address
        self.errors_to_address = errors_to_address
        self.original_recipient = original_recipient

    def validate(self):
        if not is_valid_email_address(self.address, require_fqdn=False):
            raise ValueError("'{0}' is not a valid email address".format(self.address))
        if (
            self.errors_to_address is not None
            and not is_valid_email_address(self.errors_to_address, require_fqdn=False)
        ):
            raise ValueError("'{0}' is not a valid email address".format(self.errors_to_address))


@contextlib.contextmanager
def consume_line(context):
    line = context.get_line()
    yield line
    context.consume(len(line))


class HFileParseContext(object):
    def __init__(self, source, source_name=None, start_offset=0):
        self.source = source
        self.offset = start_offset or 0
        self.source_name = source_name
        self._buffer = None

    def get_line(self, consume=False):
        """
            Retrieve a line from the input source.

            If the current buffer has not yet been fully consumed,
            the current buffer is returned. If the current
            buffer has already been fully consumed (including the trailing newline),
            a new line will be read from the input source.

            An empty buffer is returned, when the current
            buffer has been fully consumed and there is
            not more data available from the input source (EOF).
        """
        if not self._buffer:
            self._buffer = self.source.readline()
        line = self._buffer
        if consume:
            self.consume(len(line))
        return line

    def consume(self, size):
        """
            Consume up to `size` bytes from the current buffer.

            If the current buffer contains less than `size` bytes
            the whole buffer is consumed.
        """
        self.offset += min(size, len(self._buffer))
        self._buffer = self._buffer[size:]

    def read_raw(self, size):
        if self._buffer:
            raise ParseError(
                'Need to consume incomplete line buffer before raw read',
                source_name=self.source_name,
                offset=self.offset,
            )
        else:
            start_offset = self.offset
            data = self.source.read(size)
            self.offset += len(data)
            return data, start_offset


class HFile(object):
    """
        Internal representation of an exim "-H" queue data file.
    """
    def __init__(
        self,
        name,
        login, uid, gid,
        env_from,
        rcpt_timestamp,
        delay_warn_count,
    ):
        self.name = name

        # basic queue information
        self.login = login
        self.uid = uid
        self.gid = gid
        self.env_from = env_from
        self.rcpt_timestamp = rcpt_timestamp
        self.delay_warn_count = delay_warn_count

        # variables
        self.queue_vars = OrderedDict()
        self.old_acl_vars = OrderedDict()
        self.message_vars = OrderedDict()
        self.connection_vars = OrderedDict()

        # recipient information
        self.non_delivery_addresses = avltree.Tree()
        self.recipients = []

        # headers
        self.headers = []

    def validate(self, validate_env_from=True):
        if not isinstance(self.uid, int):
            raise ValidationError("Invalid uid: '{}'".format(self.uid), fields='uid')
        if not isinstance(self.gid, int):
            raise ValidationError("Invalid uid: '{}'".format(self.gid), fields='gid')
        if (
            validate_env_from
            and not is_valid_email_address(self.env_from, require_fqdn=False, accept_empty=True)
        ):
            raise ValidationError("Not a valid email address: '{}'".format(self.env_from), fields='env_from')
        if not isinstance(self.rcpt_timestamp, datetime):
            raise ValidationError(
                "Receive timestamp is not a datetime object: '{}'".format(self.rcpt_timestamp),
                'rcpt_timestamp',
            )
        if not isinstance(self.delay_warn_count, int):
            raise ValidationError("Invalid uid: '{}'".format(self.gid), fields='gid')


class HFileParser(object):
    """
        parser for exim "-H" queue data files.

    """
    def __init__(
        self,
        validate_after_parse=True,
        validate_env_from=True,
    ):
        self.validate_after_parse = validate_after_parse
        self.validate_env_from = validate_env_from

    def parse_bytes(self, b, source_name):
        """
            Parse from a byte string
        """
        source = io.BytesIO(b)
        return self.parse(source, source_name=source_name)

    def parse_string(self, s, source_name, intermediate_encoding=None):
        """
            Parse from an unicode string.

            Since the parser operators on raw byte strings, only.
            the input string will be encoded into a byte string
            using `intermediate_encoding` (default: 'utf-8').
        """
        intermediate_encoding = intermediate_encoding or 'utf-8'
        source = s.encode(intermediate_encoding)
        return self.parse_bytes(source, source_name=source_name)

    def parse_header(self, context):
        with consume_line(context) as line:
            hfile_name = line.strip()
        with consume_line(context) as line:
            user_info = line.strip()
            user_info = re.split(b'\\s+', user_info)
            if len(user_info) != 3:
                self._parse_error(
                    'Invalid sender login/uid/gid',
                    context=context,
                )
            login, uid, gid = user_info
            uid = int(uid)
            gid = int(gid)

        with consume_line(context) as line:
            env_from = line.rstrip(b'\n')
            if not env_from.startswith(b'<') or not env_from.endswith(b'>'):
                self._parse_error(
                    "Envelope sender address without '<' or '>'",
                    context=context,
                )
            env_from = env_from[1:-1]
        with consume_line(context) as line:
            rcpt_date_and_delay_warn_count = line.strip()
            rcpt_date_and_delay_warn_count = re.split(b'\\s+', rcpt_date_and_delay_warn_count)
            if len(rcpt_date_and_delay_warn_count) != 2:
                self._parse_error(
                    'Delay warning count missing',
                    context=context,
                )
            rcpt_date, delay_warn_count = rcpt_date_and_delay_warn_count
            rcpt_date = self._parse_int(rcpt_date, context)
            rcpt_date = datetime.fromtimestamp(rcpt_date, tz=pytz.UTC)
            delay_warn_count = self._parse_int(delay_warn_count, context)

        hfile = HFile(hfile_name, login, uid, gid, env_from, rcpt_date, delay_warn_count)
        if self.validate_after_parse:
            hfile.validate(validate_env_from=self.validate_env_from)
        return hfile

    def _parse_error(self, message, context, offset=None, from_value=None):
        if offset is None:
            offset = context.offset
        ex = ParseError(
            message,
            source_name=context.source_name,
            offset=offset,
        )
        if from_value is not None:
            six.raise_from(ex, from_value)
        else:
            raise ex

    def _parse_int(self, value_str, context, offset=None):
        try:
            return int(value_str)
        except ValueError as ex:
            self._parse_error(
                "Invalid integer value: '{}'".format(value_str),
                context=context, offset=offset,
                from_value=ex,
            )

    def parse_acl_variable(self, context):
        with consume_line(context) as line:
            assert line.startswith(b'-acl')
            params = re.split(b'\\s+', line.rstrip(b'\n'))
            if len(params) < 3:
                self._parse_error(
                    'Not enough parameters for ACL variable',
                    context=context
                )
            decl, var_name, data_length = params

            if decl == b'-acl':
                # old-style ACL var
                var_name = self._parse_int(var_name, context)
                var_type = 'old_acl'
            elif decl == b'-aclc':
                # connection ACL var
                try:
                    var_name = int(var_name)
                except ValueError:
                    if not var_name.startswith(b'_'):
                        self._parse_error(
                            "Missing '_' at start of ACL variable name",
                            context=context
                        )
                    var_name = var_name[1:]
                var_type = 'connection'
            elif decl == b'-aclm':
                # message ACL var
                try:
                    var_name = int(var_name)
                except ValueError:
                    if not var_name.startswith(b'_'):
                        self._parse_error(
                            "Missing '_' at start of ACL variable name",
                            context=context
                        )
                    var_name = var_name[1:]
                var_type = 'message'
            else:
                self._parse_error(
                    "Unsupported ACL variable definition: '{0}'".format(line),
                    context=context
                )

            data_length = self._parse_int(data_length, context)

        # we just left the context manager, this consumed
        # the line.
        var_value, offset = context.read_raw(data_length)
        if len(var_value) != data_length:
            self._parse_error(
                'Not enough data to fill variable, got {0}, wanted {1}'.format(len(var_value), data_length),
                context=context, offset=offset
            )

        with consume_line(context) as line:
            # check for terminator
            if line != b'\n':
                self._parse_error(
                    'Missing line terminator after variable value',
                    context=context
                )

        return var_type, var_name, var_value

    def parse_queue_variable(self, context):
        with consume_line(context) as line:
            assert line.startswith(b'-')
            parts = re.split(b'\\s+', line[1:].rstrip(b'\n'))
            var_name, params = parts[0], parts[1:]
            return var_name, params

    def parse_variables(self, hfile, context):
        line = context.get_line()
        while line.startswith(b'-'):
            # we have a variable
            var_def = line[1:]
            if var_def.startswith(b'acl'):
                var_type, var_name, var_value = self.parse_acl_variable(context)
                getattr(hfile, var_type + '_vars')[var_name] = var_value
            else:
                var_name, params = self.parse_queue_variable(context)
                hfile.queue_vars[var_name] = params
            line = context.get_line()

    def _parse_nda_tree(self, context):
        with consume_line(context) as line:
            addresses = []
            parts = re.split(b'\\s+', line.rstrip(b'\n'), 1)
            if len(parts) < 2:
                self._parse_error(
                    "Delivery address without prefix: '{}'".format(line),
                    context=context,
                )
            meta, address = parts
            if meta not in (b'NN', b'NY', b'YN', b'YY'):
                self._parse_error(
                    "Invalid prefix for non-delivery address: '{0}'".format(meta),
                    context=context,
                )

            addresses.append(address)
        # leaving context manager, consuming line
        if meta[0:1] == b'Y':
            addresses.extend(self._parse_nda_tree(context))
        if meta[1:2] == b'Y':
            addresses.extend(self._parse_nda_tree(context))
        return addresses

    def parse_non_delivery_addresses(self, hfile, context):
        line = context.get_line()
        if line.startswith(b'XX'):
            context.consume(len(line))
        elif line.startswith((b'NN', b'YN', b'YY', b'NY')):
            addresses = self._parse_nda_tree(context)
            hfile.non_delivery_addresses.extend(addresses)

    def _build_recipient(self, context, address, errors_to_address=None, original_recipient=None):
        rcpt = HFileRecipient(
            address=address,
            errors_to_address=errors_to_address,
            original_recipient=original_recipient
        )
        try:
            rcpt.validate()
            return rcpt
        except ValueError as ex:
            self._parse_error(
                'Invalid recipient: {}'.format(address),
                context=context,
                from_value=ex,
            )

    def _parse_recipient(self, recipients, context):
        with consume_line(context) as line:
            addr_spec = line.rstrip(b'\n')
            match = re.search(b'^(?P<address>.*) \\d+,(?P<parent_no>\\d+,),\\d+$', addr_spec)
            if match:
                # exim 3 queue files
                address = match.group('match')
                parent_no = self._parse_int(match.group('parent_no'), context)
                parent_addr = recipients[parent_no]
                rcpt = self._build_recipient(context, address, original_recipient=parent_addr)
                return rcpt

            match = re.search(b'^(?P<address>.*) (?P<parent_no>\\d+)$', addr_spec)
            if match:
                # early exim 4 queue files
                address = match.group('match')
                parent_no = self._parse_int(match.group('parent_no'), context)
                parent_addr = recipients[parent_no]
                rcpt = self._build_recipient(context, address, original_recipient=parent_addr)
                return rcpt

            match = re.search(b' (?P<len>\\d+),(?P<pno>\\d+)#(?P<flags>\\d+)$', addr_spec)
            if match:
                length = self._parse_int(match.group('len'), context)
                parent_no = self._parse_int(match.group('parent_no'), context)
                flags = self._parse_int(match.group('flags'), context)
                if flags & 0x02:
                    self._parse_error(
                        'Original recipient and DSN flags are not implemented',
                        context=context
                    )
                if not flags & 0x01:
                    self._parse_error(
                        'Unsupported flags, least significant bit not set',
                        context=context
                    )

                errors_to_start = match.start() - length
                if errors_to_start < 0:
                    self._parse_error(
                        "Address length to large",
                        context=context
                    )
                errors_to = line[errors_to_start, match.start()]
                if line[errors_to_start - 1] != ord(b' '):
                    self._parse_error(
                        "Missing space separating addresses",
                        context=context
                    )
                address = line[0:errors_to_start]
                parent_addr = recipients[parent_no]
                rcpt = self._build_recipient(
                    context,
                    address,
                    errors_to_address=errors_to,
                    original_recipient=parent_addr
                )
                return rcpt

            rcpt = self._build_recipient(context, line.rstrip(b'\n'))
            return rcpt

    def parse_recipients(self, hfile, context):
        line = context.get_line()
        nrecipients = self._parse_int(line.rstrip(b'\n'), context)
        context.consume(len(line))
        recipients = [None] * nrecipients
        for i in range(0, nrecipients):
            recipients[i] = self._parse_recipient(recipients, context)
        assert all(recipients)
        hfile.recipients[:] = recipients

    def parse_message_header(self, context):
        c, offset = context.read_raw(1)

        if c == b'':
            # EOF
            return None

        if not c.isdigit():
            self._parse_error(
                'Header line does not start with length',
                context, offset=offset,
            )
        length = b''
        while c.isdigit():
            length += c
            c, offset = context.read_raw(1)
        length = self._parse_int(length, context, offset=offset)
        header_flag = c
        if header_flag not in HFileHeader.HEADER_FLAGS:
            self._parse_error(
                "Unknown header flag: '{}'".format(header_flag),
                context=context, offset=offset
            )
        c, offset = context.read_raw(1)
        if not c == b' ':
            self._parse_error(
                'Missing space separator in header',
                context=context, offset=offset
            )
        header_data, offset = context.read_raw(length)
        if len(header_data) != length:
            self._parse_error(
                "Unable to read full header, wanted {} bytes, got {}".format(length, len(header_data)),
                context=context, offset=offset
            )
        if not header_data[-1:] == b'\n':
            self._parse_error(
                "Missing terminating newline in header data",
                context=context, offset=offset
            )

        # we keep the trailing newlines, even though they are
        # bare newlines instead of the CRLF that are required
        # by RFC 5322
        header_field, header_value = header_data.split(b':', 1)
        # header_value will usually have a leading space (": ").
        # According to RFC 5322 this is indeed part of the value,
        # so keep it (most value definitions allow for CFWS at the start).
        header = HFileHeader(header_flag, header_field, header_value)
        return header

    def parse_message_headers(self, context):
        with consume_line(context) as line:
            if line != b'\n':
                self._parse_error(
                    'Missing separator line before headers',
                    context=context
                )
        headers = []
        header = self.parse_message_header(context)
        while header:
            headers.append(header)
            header = self.parse_message_header(context)
        return headers

    def build_context(self, source, source_name):
        context = HFileParseContext(source, source_name=source_name)
        return context

    def parse(self, source, source_name):
        """
            - `source` must be a stream-like object supporting
              both a `.readline()` and `.read()` methods.
            - `source_name` represents the name of the source
              object to be used in error messages and other output.
              Defaults to `source`.
        """
        context = self.build_context(source, source_name)
        hfile = self.parse_header(context)
        self.parse_variables(hfile, context)
        self.parse_non_delivery_addresses(hfile, context)
        self.parse_recipients(hfile, context)
        headers = self.parse_message_headers(context)
        hfile.headers.extend(headers)

        return hfile
