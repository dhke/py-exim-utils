# -*- encoding=utf-8 -*-

from __future__ import print_function

__all__ = [
    'HFileFormatter',
]

import datetime
import pytz
import six


class HFileFormatter(object):
    DEFAULT_FIELDS = [
        'name',
        'age',
        'env_from',
        'recipients',
    ]
    DEFAULT_FIELD_FORMATS = {
        'name': '{:18}',
        'env_from': '{:40}',
        'recipients': '<{}>',  # per recipient!
        'age': '{:25}',
    }
    DEFAULT_FORMAT = '{}'

    def __init__(
        self,
        fields=None,
        field_formats=None,
        default_format=None,
        sep='\t',
        timestamp=None,
    ):
        self.fields = fields or self.DEFAULT_FIELDS
        self.field_formats = field_formats or self.DEFAULT_FIELD_FORMATS
        self.default_format = default_format or self.DEFAULT_FORMAT
        self.sep = sep
        self.timestamp = timestamp or datetime.datetime.now(pytz.utc)

    def format(self, hfile):
        field_values = [
            self.format_field(field, hfile)
            for field in self.fields
        ]
        line = self.sep.join(field_values)
        return line

    def format_str(self, field, value):
        fmt = self.field_formats.get(field, self.default_format)
        if isinstance(value, six.binary_type):
            value = value.decode('ascii')
        return fmt.format(str(value))

    def format_default(self, field, hfile):
        value = getattr(hfile, field)
        return self.format_str(field, value)

    def format_recipients(self, field, hfile):
        return ', '.join(self.format_str(field, rcpt.address) for rcpt in hfile.recipients)

    def format_field(self, field, hfile):
        field_formatter = getattr(self, 'format_{}'.format(field), None)
        if not callable(field_formatter):
            field_formatter = self.format_default
        return field_formatter(field, hfile)

    def format_env_from(self, field, hfile):
        value = getattr(hfile, field)
        value = '<{}>'.format(value.decode('ascii'))
        return self.format_str(field, value)

    def format_age(self, field, hfile):
        delta = self.timestamp - hfile.rcpt_timestamp
        return self.format_str(field, delta)

    def format_name(self, field, hfile):
        value = getattr(hfile, field)
        if value.endswith(b'-H'):
            value = value[:-2]
        return self.format_str(field, value)
