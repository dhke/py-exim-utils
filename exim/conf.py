# -*- encoding=utf-8 -*-

import os
import six
import subprocess


__all__ = [
    'DEFAULT_SEARCH_PATH',
    'DEFAULT_EXIM_BINARY',
    'CONF_SPOOLDIR'
    'find_exim',
    'get_conf',
]


DEFAULT_EXIM_BINARY = 'exim'
DEFAULT_SEARCH_PATH = [
    '/usr/sbin',
    '/usr/local/sbin',
]

CONF_SPOOLDIR = 'spool_directory'


def find_exim(exim_binary=None, search_path=None):
    if not exim_binary:
        exim_binary = DEFAULT_EXIM_BINARY
    if not search_path:
        search_path = DEFAULT_SEARCH_PATH
    for path in search_path:
        binary_path = os.path.join(path, exim_binary)
        if os.access(binary_path, os.X_OK):
            return binary_path
    return exim_binary


def get_conf(name, exim_binary=None, search_path=None):
    exim_binary = find_exim(exim_binary=exim_binary, search_path=search_path)
    cmd_args = [exim_binary, '-n', '-bP', name]
    try:
        value = subprocess.check_output(cmd_args)
        value = value.rstrip('\n')
        return value
    except subprocess.CalledProcessError as ex:
        six.raise_from(
            IOError("Unable to obtain exim configuration variable '{}'".format(name)),
            ex
        )
