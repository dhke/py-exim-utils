# -*- encoding=utf-8 -*-

import calendar
import unittest
import six

from io import BytesIO

from exim.queue.hfile import ParseError, HFile, HFileParser


def to_timestamp(datetime):
    if six.PY2:
        return calendar.timegm(datetime.timetuple())
    else:
        return datetime.timestamp()


class TestHFileParser(unittest.TestCase):
    def setUp(self):
        self.parser = HFileParser()

    def tearDown(self):
        self.parser = None

#######################
# header parsing tests
#######################
    def test_parse_header(self):
        name = b'test_parse_header'
        data = BytesIO(b'''test_parse_header
root 0 0
<root@exim>
1234567890 1''')
        context = self.parser.build_context(data, source_name=name)
        hfile = self.parser.parse_header(context)
        self.assertEqual(name, hfile.name)
        self.assertEqual(b'root', hfile.login)
        self.assertEqual(0, hfile.uid)
        self.assertEqual(0, hfile.gid)
        self.assertEqual(b'root@exim', hfile.env_from)
        self.assertEqual(1234567890, int(to_timestamp(hfile.rcpt_timestamp)))
        self.assertEqual(1, hfile.delay_warn_count)
        self.assertFalse(hfile.old_acl_vars)
        self.assertFalse(hfile.queue_vars)
        self.assertFalse(hfile.message_vars)
        self.assertFalse(hfile.connection_vars)
        self.assertFalse(hfile.non_delivery_addresses)

    def test_parse_header_nogid(self):
        name = b'test_parse_header_nogid'
        data = BytesIO(b'''test_parse_header_nogid
root 0
<root@exim>
1234567890 1''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_header(context)

    def test_parse_header_nouid(self):
        name = b'test_parse_header_nouid'
        data = BytesIO(b'''test_parse_header_nouid
root
<root@exim>
1234567890 1''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_header(context)

    def test_parse_header_no_warn_count(self):
        name = b'test_parse_header_no_warn_count'
        data = BytesIO(b'''test_parse_header_no_warn_count
root 0 0
<root@exim>
1234567890''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_header(context)

    def test_parse_header_invalid_date(self):
        name = b'test_parse_header_invalid_date'
        data = BytesIO(b'''test_parse_header_invalid_date
root 0 0
<root@exim>
123bla 1''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_header(context)

    def test_parse_header_invalid_warn_count(self):
        name = b'test_parse_header_invalid_warn_count'
        data = BytesIO(b'''test_parse_header_invalid_warn_count
root 0 0
<root@exim>
12334567890 1bla''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_header(context)


############
# variables
############
    def test_parse_variables(self):
        name = b'test_parse_variables'
        data = BytesIO(b'''-received_time_usec 0.1234
-received_protocol local
-aclc _var1 6
value1
-aclm _var2 7
value22
-aclc 1 5
value
-aclm 2 7
value33
-auth_id root
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_variables(hfile, context)

        self.assertDictEqual(hfile.queue_vars, {
            b'received_time_usec': [b'0.1234', ],
            b'received_protocol': [b'local', ],
            b'auth_id': [b'root', ],
        })
        self.assertDictEqual(hfile.connection_vars, {
            b'var1': b'value1',
            1: b'value',
        })
        self.assertDictEqual(hfile.message_vars, {
            b'var2': b'value22',
            2: b'value33',
        })

    def test_parse_variables_value_too_short(self):
        name = b'test_parse_variables_value_too_short'
        data = BytesIO(b'''-aclc _var1 7
value1
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        with self.assertRaises(ParseError):
            self.parser.parse_variables(hfile, context)

    def test_parse_variables_value_too_long(self):
        name = b'test_parse_variables_value_too_short'
        data = BytesIO(b'''-aclc _var1 5
value1
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        with self.assertRaises(ParseError):
            self.parser.parse_variables(hfile, context)

##########################
# non-delivery addressses
##########################
    def test_parse_non_delivery_addresses(self):
        name = b'test_parse_non_delivery_addresses'
        data = BytesIO(b'''YY exim4@exim
NY exim2@exim
NN exim3@exim
YN exim6@exim
NN exim5@exim
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_non_delivery_addresses(hfile, context)
        if six.PY3:
            self.assertCountEqual(
                [b'exim2@exim', b'exim3@exim', b'exim4@exim', b'exim5@exim', b'exim6@exim'],
                hfile.non_delivery_addresses
            )
        else:
            self.assertItemsEqual(
                [b'exim2@exim', b'exim3@exim', b'exim4@exim', b'exim5@exim', b'exim6@exim'],
                hfile.non_delivery_addresses
            )

    def test_parse_non_delivery_addresses_missing_leaf(self):
        name = b'test_parse_non_delivery_addresses_missing_leaf'
        data = BytesIO(b'''YY exim4@exim
NY exim2@exim
NN exim3@exim

''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        with self.assertRaises(ParseError):
            self.parser.parse_non_delivery_addresses(hfile, context)

    def test_parse_non_delivery_addresses_too_many_nodes(self):
        name = b'test_parse_non_delivery_addresses_too_many_nodes'
        data = BytesIO(b'''NN exim4@exim
NN exim3@exim

''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_non_delivery_addresses(hfile, context)
        with self.assertRaises(ParseError):
            self.parser.parse_recipients(hfile, context)

    def test_parse_non_delivery_addresses_left(self):
        name = b'test_parse_non_delivery_addresses'
        data = BytesIO(b'''YN exim2@exim
NN exim1@exim
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_non_delivery_addresses(hfile, context)
        if six.PY3:
            self.assertCountEqual(
                [b'exim1@exim', b'exim2@exim'],
                hfile.non_delivery_addresses
            )
        else:
            self.assertItemsEqual(
                [b'exim1@exim', b'exim2@exim'],
                hfile.non_delivery_addresses
            )

    def test_parse_non_delivery_addresses_right(self):
        name = b'test_parse_non_delivery_addresses'
        data = BytesIO(b'''NY exim1@exim
NN exim2@exim
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_non_delivery_addresses(hfile, context)
        if six.PY3:
            self.assertCountEqual(
                [b'exim1@exim', b'exim2@exim'],
                hfile.non_delivery_addresses
            )
        else:
            self.assertItemsEqual(
                [b'exim1@exim', b'exim2@exim'],
                hfile.non_delivery_addresses
            )

#################
# recipient list
#################
    def test_parse_recipients(self):
        name = b'test_parse_recipients'
        data = BytesIO(b'''4
exim1@exim
exim2@exim
exim3@exim
exim4@exim

''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_recipients(hfile, context)
        self.parser.parse_message_headers(context)
        if six.PY3:
            self.assertCountEqual(
                [b'exim1@exim', b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                [recipient.address for recipient in hfile.recipients]
            )
        else:
            self.assertItemsEqual(
                [b'exim1@exim', b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                [recipient.address for recipient in hfile.recipients]
            )

    def test_parse_recipients_too_many(self):
        name = b'test_parse_recipients_too_many',
        data = BytesIO(b'''3
exim1@exim
exim2@exim
exim3@exim
exim4@exim
''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        self.parser.parse_recipients(hfile, context)
        with self.assertRaises(ParseError):
            self.parser.parse_message_headers(context)

    def test_parse_recipients_too_few(self):
        name = b'test_parse_recipients_too_few',
        data = BytesIO(b'''4
exim1@exim
exim2@exim
exim3@exim

''')
        context = self.parser.build_context(data, source_name=name)
        hfile = HFile(name, None, None, None, None, None, None)
        with self.assertRaises(ParseError):
            self.parser.parse_recipients(hfile, context)

    def test_parse_message_headers(self):
        name = 'test_parse_message_headers'
        ref_headers = [
            b'''159P Received: from root by exim with local (Exim 4.91 (FreeBSD))
\t(envelope-from <root@exim>)
\tid 1fsMeC-000LrH-5P
\tfor exim@exim; Wed, 22 Aug 2018 08:29:40 +0200
''',
            b'009* To: exim\n',
            b'014T To: exim@exim\n',
        ]
        data = BytesIO(b'\n' + b''.join(ref_headers))
        context = self.parser.build_context(data, source_name=name)
        headers = self.parser.parse_message_headers(context)
        for ref_header, header in zip(ref_headers, headers):
            self.assertEqual(ref_header, header.bytes())

    def test_parse_headers_too_short(self):
        name = 'test_parse_message_headers'
        data = BytesIO(b'''008* To: exim
014T To: exim@exim
''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_message_headers(context)

    def test_parse_headers_too_long1(self):
        name = 'test_parse_message_headers'
        data = BytesIO(b'''010* To: exim
014T To: exim@exim
''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_message_headers(context)

    def test_parse_headers_too_long2(self):
        name = 'test_parse_message_headers'
        data = BytesIO(b'''009* To: exim
015T To: exim@exim
''')
        context = self.parser.build_context(data, source_name=name)
        with self.assertRaises(ParseError):
            self.parser.parse_message_headers(context)


############################
# complete hfile parse test
############################
    def test_parse_hfile(self):
        name = b'test_parse_hfile'
        data = b'''test_parse_hfile
root 0 0
<root@exim>
1534919380 0
-received_time_usec .167772
-ident root
-received_protocol local
-aclc _var1 7
"var 1"
-aclc _othervar 11
"other var"
-aclm _mvar1 9
"value 2"
-aclm _mvar2 9
"value 4"
-body_linecount 1
-max_received_linelength 21
-auth_id root
-auth_sender root@exim
-allow_unqualified_recipient
-allow_unqualified_sender
-local
YY exim3@exim
NN exim2@exim
NN exim4@exim
4
exim@exim
exim2@exim
exim3@exim
exim4@exim

159P Received: from root by exim with local (Exim 4.91 (FreeBSD))
\t(envelope-from <root@exim>)
\tid 1fsMeC-000LrH-5P
\tfor exim@exim; Wed, 22 Aug 2018 08:29:40 +0200
009* To: exim
014T To: exim@exim
022  Subject: test message
037I Message-Id: <E1fsMeC-000LrH-5P@exim>
031F From: Charlie Root <root@exim>
038  Date: Wed, 22 Aug 2018 08:29:40 +0200
'''
        hfile = self.parser.parse_bytes(data, source_name=name)
        self.assertEqual(name, hfile.name)
        self.assertEqual(0, hfile.uid)
        self.assertEqual(0, hfile.gid)
        self.assertEqual(b'root', hfile.login)
        self.assertEqual(b'root@exim', hfile.env_from)
        self.assertEqual(1534919380, int(to_timestamp(hfile.rcpt_timestamp)))
        self.assertEqual(0, hfile.delay_warn_count)
        self.assertDictEqual({
            b'received_time_usec': [b'.167772'],
            b'ident': [b'root'],
            b'received_protocol': [b'local'],
            b'body_linecount': [b'1'],
            b'max_received_linelength': [b'21'],
            b'auth_id': [b'root'],
            b'auth_sender': [b'root@exim'],
            b'allow_unqualified_recipient': [],
            b'allow_unqualified_sender': [],
            b'local': [],
            },
            hfile.queue_vars
        )
        self.assertDictEqual({
            b'var1': b'"var 1"',
            b'othervar': b'"other var"',
            },
            hfile.connection_vars
        )
        self.assertDictEqual({
            b'mvar1': b'"value 2"',
            b'mvar2': b'"value 4"',
            },
            hfile.message_vars
        )
        if six.PY3:
            self.assertCountEqual(
                [b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                hfile.non_delivery_addresses
            )
            self.assertCountEqual(
                [b'exim@exim', b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                [rcpt.address for rcpt in hfile.recipients]
            )
        else:
            self.assertItemsEqual(
                [b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                hfile.non_delivery_addresses,
            )
            self.assertItemsEqual(
                [b'exim@exim', b'exim2@exim', b'exim3@exim', b'exim4@exim'],
                [rcpt.address for rcpt in hfile.recipients]
            )


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestHFileParser))
    return suite


if __name__ == '__main__':
    unittest.main()
