# -*- encoding=utf-8 -*-

import unittest
from io import BytesIO

from exim.db import lsearch


class ParseHandler(lsearch.LFileParseHandler):
    def __init__(self):
        self.data_events = []
        self.comment_events = []
        self.value_events = []
        self.key_events = []

    def on_key(self, key, index, source=None, lineno=None):
        self.key_events.append((key, index, source, lineno))

    def on_value(self, value, index, source=None, lineno=None):
        self.value_events.append((value, index, source, lineno))

    def on_data(self, key, value, source=None, lineno=None):
        self.data_events.append((key, value, source, lineno))

    def on_comment(self, comment, source=None, lineno=None):
        self.comment_events.append((comment, source, lineno))


class TestLFileParserBase:
    def setUp(self):
        self.handler = ParseHandler()
#        self.parser = lsearch.LFileParser(self.handler, debug=True)
        self.parser = lsearch.LFileParser(self.handler)

    def tearDown(self):
        self.handler = None
        self.parser = None


class TestLFileParser(TestLFileParserBase, unittest.TestCase):
    def test_simple_key(self):
        s = 'key'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', None, s, 1)],
            self.handler.data_events
        )

    def test_simple_key2(self):
        s = 'key1\nkey2'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key1', None, s, 1), (b'key2', None, s, 2)],
            self.handler.data_events
        )

    def test_simple_kv_space(self):
        s = 'key value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'value', s, 1)],
            self.handler.data_events
        )

    def test_simple_kv_colon(self):
        s = 'key:value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'value', s, 1)],
            self.handler.data_events
        )

    def test_simple_kv_strip_space(self):
        s = 'key: value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'value', s, 1)],
            self.handler.data_events
        )

    def test_simple_kv_strip_nl(self):
        s = 'key: value\n'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'value', s, 1)],
            self.handler.data_events
        )

    def test_quoted_key(self):
        s = '"k y"'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'k y', None, s, 1)],
            self.handler.data_events
        )

    def test_quoted_kv_space(self):
        s = '"k y" value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'k y', b'value', s, 1)],
            self.handler.data_events
        )

    def test_quoted_kv_colon(self):
        s = '"k y:":value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'k y:', b'value', s, 1)],
            self.handler.data_events
        )

    def test_quoted_kv_strip_space(self):
        s = '"key ": value'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key ', b'value', s, 1)],
            self.handler.data_events
        )

    def test_quoted_kv_embedded_nl(self):
        s = '"key\\n": value\n'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key\n', b'value', s, 1)],
            self.handler.data_events
        )

    def test_comment(self):
        s = '# comment'
        self.parser.parse_string(s)
        self.assertEqual([(b' comment', s, 1)], self.handler.comment_events)
        self.assertEqual([], self.handler.data_events)

    def test_comment_with_continuation(self):
        s = 'key: value1\n# comment\n value2'
        self.parser.parse_string(s)
        self.assertEqual([(b' comment', s, 2)], self.handler.comment_events)
        self.assertEqual([(b'key', b'value1 value2', s, 1)], self.handler.data_events)

    def test_continuation_before_value(self):
        s = 'key:\n valueline1'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'valueline1', s, 1)],
            self.handler.data_events
        )

    def test_continuation_before_value_space(self):
        s = 'key: \n valueline1'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b' valueline1', s, 1)],
            self.handler.data_events
        )

    def test_continuation_in_value(self):
        s = 'key: valueline1\n  valueline2'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'valueline1 valueline2', s, 1)],
            self.handler.data_events
        )

    def test_tab_continuation_before_value(self):
        s = 'key:\n\tvalueline1'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'valueline1', s, 1)],
            self.handler.data_events
        )

    def test_tab_continuation_in_value(self):
        s = 'key: valueline1\n\tvalueline2'
        self.parser.parse_string(s)
        self.assertEqual([], self.handler.comment_events)
        self.assertEqual(
            [(b'key', b'valueline1 valueline2', s, 1)],
            self.handler.data_events
        )


class TestLFileParserComplex(TestLFileParserBase, unittest.TestCase):
    def test_complex_input1(self):
        s = '''"k y1": valueline1
    valueline2
key2: value21 value22
# comment1
key3: value31
# comment2
    value32
key4:
# comment3
 value4
'''
        self.parser.parse_string(s)
        self.assertEqual(
            [
                (b' comment1', s, 4),
                (b' comment2', s, 6),
                (b' comment3', s, 9)
            ],
            self.handler.comment_events
        )
        self.assertEqual(
            [
                (b'k y1', b'valueline1 valueline2', s, 1),
                (b'key2', b'value21 value22', s, 3),
                (b'key3', b'value31 value32', s, 5),
                (b'key4', b'value4', s, 8)
            ],
            self.handler.data_events
        )


class TestLFileParserError(TestLFileParserBase, unittest.TestCase):
    def test_unclosed_quote(self):
        s = '"key'
        with self.assertRaises(lsearch.ParseError):
            self.parser.parse_string(s)

    def test_continuation_at_start(self):
        s = ' key'
        with self.assertRaises(lsearch.ParseError):
            self.parser.parse_string(s)

    def test_space_before_comment(self):
        s = ' # comment'
        with self.assertRaises(lsearch.ParseError):
            self.parser.parse_string(s)


class TestLFileWriter(unittest.TestCase):
    def setUp(self):
        self.target = BytesIO()
        self.writer = lsearch.LFileWriter(target=self.target)

    def tearDown(self):
        self.writer = None
        self.target = None

    def test_write_key(self):
        self.writer.write_data(b'key')
        self.assertEqual(b'key\n', self.target.getvalue())

    def test_write_key_vale(self):
        self.writer.write_data(b'key', b'value')
        self.assertEqual(b'key:value\n', self.target.getvalue())

    def test_write_quoted_key_value(self):
        self.writer.write_data(b'k y', b'value')
        self.assertEqual(b'"k y":value\n', self.target.getvalue())

    def test_write_comment(self):
        self.writer.write_comment(b' a b c')
        self.assertEqual(b'# a b c\n', self.target.getvalue())

    def test_write_multiline_comment(self):
        self.writer.write_comment(b'a\nb\r\nc')
        self.assertEqual(b'#a\n#b\n#c\n', self.target.getvalue())


class TestLFileWriterEncoded(unittest.TestCase):
    def setUp(self):
        self.target = BytesIO()
        self.writer = lsearch.LFileWriter(target=self.target, encoding='utf-8')

    def tearDown(self):
        self.writer = None
        self.target = None

    def test_write_key(self):
        self.writer.write_data('key')
        self.assertEqual(b'key\n', self.target.getvalue())

    def test_write_key_vale(self):
        self.writer.write_data('key', 'value')
        self.assertEqual(b'key:value\n', self.target.getvalue())

    def test_write_quoted_key_value(self):
        self.writer.write_data('k y', 'value')
        self.assertEqual(b'"k y":value\n', self.target.getvalue())

    def test_write_comment(self):
        self.writer.write_comment(' a b c')
        self.assertEqual(b'# a b c\n', self.target.getvalue())

    def test_write_multiline_comment(self):
        self.writer.write_comment('a\nb\r\nc')
        self.assertEqual(b'#a\n#b\n#c\n', self.target.getvalue())


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestLFileParser))
    suite.addTest(unittest.makeSuite(TestLFileParserComplex))
    suite.addTest(unittest.makeSuite(TestLFileParserError))
    suite.addTest(unittest.makeSuite(TestLFileWriter))
    suite.addTest(unittest.makeSuite(TestLFileWriterEncoded))
    return suite


if __name__ == '__main__':
    unittest.main()
