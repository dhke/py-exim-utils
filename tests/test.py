# -*- encoding=utf-8 -*-

import importlib
import unittest
import sys


def suite():
    test_modules = [
        'tests.test_avltree',
        'tests.test_lsearch',
        'tests.test_hfile_parser',
        'tests.test_validate',
    ]
    suite = unittest.TestSuite()
    for test_module in test_modules:
        mod = importlib.import_module(test_module)
        suite.addTest(mod.suite())
    return suite


if __name__ == '__main__':
    unittest.main()
