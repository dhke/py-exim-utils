# -*- encoding=utf-8 -*-

import unittest

from exim.util import validate


class TestValidateEmailAddress(unittest.TestCase):
    def is_valid_email_address(self):
        self.assertTrue(validate.is_valid_email_address(b''))
        self.assertTrue(validate.is_valid_email_address(b'a@example.org'))
        self.assertTrue(validate.is_valid_email_address(b'A@example.org'))
        self.assertTrue(validate.is_valid_email_address(b'Ab@example.org'))
        self.assertTrue(validate.is_valid_email_address(b'aB@example.org'))
        self.assertTrue(validate.is_valid_email_address(b'123@example.org'))
        self.assertTrue(validate.is_valid_email_address(b'a@12example.org'))
        self.assertTrue(validate.is_valid_email_address(b'a@12example.org'))
        self.assertTrue(validate.is_valid_email_address(b'a@12example.com'))
        self.assertTrue(validate.is_valid_email_address(b'a@example', require_fqdn=False))
        self.assertTrue(validate.is_valid_email_address(b'" "@example', require_fqdn=False))

    def is_invalid_email_address(self):
        self.assertFalse(validate.is_valid_email_address(b'@'))
        self.assertFalse(validate.is_valid_email_address(b'a'))
        self.assertFalse(validate.is_valid_email_address(b'a@'))
        self.assertFalse(validate.is_valid_email_address(b'ab@'))
        self.assertFalse(validate.is_valid_email_address(b'@example'))
        self.assertFalse(validate.is_valid_email_address(b'@example', require_fqdn=False))
        self.assertFalse(validate.is_valid_email_address(b'@example.org', require_fqdn=False))


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestValidateEmailAddress))
    return suite


if __name__ == '__main__':
    unittest.main()
