# -*- encoding=utf-8 -*-

import itertools
import unittest

from exim.util import avltree


def get_height(node, height_map=None):
    if height_map is None:
        height_map = {}
    if node is None:
        return 0

    assert len(node.children) == 2
    if node.children == [None, None]:
        height_map[node] = 1
    else:
        height_map[node] = max(get_height(child, height_map=height_map) for child in node.children) + 1
    return height_map[node]


class TestTreeInsert(unittest.TestCase):
    def validate_tree(self, tree):
        height_map = {}
        root = tree.root.children[0]
        if root is None:
            return
        for node in tree.node_iter():
            child_height = [get_height(child, height_map=height_map) for child in node.children]
            self.assertEqual(child_height[1] - child_height[0], node.balance)

    def test_add_shuffle(self):
#        items = ('a', 'b', 'c', 'd', 'e', 'f', 'g')
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        for shuffled in itertools.permutations(items):
            tree = avltree.Tree()
            for n, v in enumerate(shuffled, start=1):
                tree.add(v)
                self.assertEqual(n, len(tree))
                self.assertEqual(tuple(sorted(shuffled[0:n])), tuple(tree))
                self.validate_tree(tree)
            self.assertEqual(items, tuple(tree))

    def test_remove_shuffle(self):
#        items = ('a', 'b', 'c', 'd', 'e', 'f', 'g')
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        tree = avltree.Tree()
        for shuffled in itertools.permutations(items):
            tree = avltree.Tree()
            for v in items:
                tree.add(v)
            self.assertEqual(items, tuple(tree))
            for n, item in enumerate(shuffled, 1):
                tree.remove(item)
                self.assertEqual(len(shuffled) - n, len(tree))
                self.assertEqual(tuple(sorted(shuffled[n:])), tuple(tree))
                self.validate_tree(tree)

    def test_iter(self):
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        tree = avltree.Tree()
        for item in items:
            tree.add(item)
        self.assertEqual(items, tuple(tree))

    def test_in(self):
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        tree = avltree.Tree(items)
        for item in items:
            self.assertIn(item, tree)

    def test_not_in(self):
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        not_items = ('1', '2', '3', '4')
        tree = avltree.Tree(items)
        for item in not_items:
            self.assertNotIn(item, tree)

    def test_sum(self):
        items1 = ('a', 'c', 'e')
        items2 = ('b', 'd', 'f')
        tree = avltree.Tree(items1)
        tree2 = tree + items2
        self.assertIsNot(tree, tree2)
        self.assertEqual(('a', 'b', 'c', 'd', 'e', 'f'), tuple(tree2))

    def test_extend(self):
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        tree = avltree.Tree()
        tree.extend(items)
        self.assertEqual(items, tuple(tree))

    def test_init(self):
        items = ('a', 'b', 'c', 'd', 'e', 'f')
        tree = avltree.Tree(items)
        self.assertEqual(items, tuple(tree))

    def test_bool(self):
        tree = avltree.Tree()
        self.assertFalse(tree)
        tree.add('a')
        self.assertTrue(tree)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestTreeInsert))
    return suite


if __name__ == '__main__':
    unittest.main()
