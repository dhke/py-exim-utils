%{?!python_module:%define python_module() python-%{**} python3-%{**}}

Name:           python-exim-utils
Version:        0.1.8
Release:        0
Summary:        Python module for interactive with the exim mailer
License:        Artistic
Group:          Development/Languages/Python
Url:            https://gitlab.com/dhke/py-exim-utils/
Source:         https://gitlab.com/dhke/py-exim-utils/-/archive/%{version}/py-exim-utils-%{version}.tar.bz2

BuildRequires:  %{python_module devel}
BuildRequires:  %{python_module setuptools}
BuildRequires:  %{python_module six}
BuildRequires:  %{python_module pytz}
BuildRequires:  fdupes
BuildRequires:  python-rpm-macros

Requires:       python-six
Requires:       python-pytz

%python_subpackages

%description
A python library and a set of utilities to interact
with the exim mail transfer agent (MTA).

%build
%python_build

%install
%python_install
%python_expand %fdupes %{buildroot}%{python_sitelib}

%check
%python_exec setup.py test

%files %{python_files}
%{python_sitelib}/*

%changelog
